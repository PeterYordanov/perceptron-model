use std::vec::Vec;

#[derive(Clone)]
pub struct Perceptron {
    eta: f64,
    epochs: f64,
    weights: Vec<f64>,
    errors: Vec<f64>
}

fn print_vector(vector: Vec<f64>) {
    println!("{:?}", vector);
}

impl Perceptron {
    pub fn new(eta: f64, epochs: f64) -> Perceptron {
        Perceptron { 
            eta, 
            epochs, 
            weights: Vec::new(),
            errors: Vec::new()
        }
    }

    // Sum(Vector of weights * Input vector) + bias
    pub fn net_input(self, X: &mut Vec<f64>) -> f64 {
        let mut probabilities: f64 = *self.weights.first().unwrap();
        
        let mut size = X.len();

        for i in 0..size {
            probabilities += X[i as usize] * self.weights[(i as usize + 1) as usize];
        }
        
        probabilities
    }

    pub fn predict(self, X: &mut Vec<f64>) -> f64 {
        if self.net_input(X) > 0.0 { 1.0 } else { -1.0 }
    }

    pub fn fit(&mut self, X: &mut Vec<Vec<f64>>, y: Vec<f64>) {
        let mut size = (*X.first().unwrap()).len();

        //Setting each weight to 0 and making the size of the vector
        //The same as the number of features (X[0].size()) + 1 for the bias term
        //+1 is for the bias term
        for i in 0..(size + 1) {
            self.weights.push(0.0);
        }

        for i in 0..self.epochs as i32 {
            let mut error: i32 = 0;

            for j in 0..X.len() {
                let update: f64 = self.clone().eta * (y[j] - self.clone().predict(&mut X[j]));
                
                for w in 0..self.weights.len() {
                    self.weights[w] += update * X[j][w - 1];
                }

                self.weights[0] = update;
                error += if update != 0.0 { 1 } else { 0 };
            }

            self.errors.push(error as f64);
        }
    }

    pub fn print_errors(self) {
        print_vector(self.errors);
    }

    pub fn print_weights(self) {
        print_vector(self.weights);
    }

    pub fn import_weights(self) {

    }

    pub fn export_weights(self) {

    }
}